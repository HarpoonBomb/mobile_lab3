package com.example.olejacob.mobdev_lab3;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.math.MathUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.TextureView;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public Integer width;
    public Integer height;
    public final Integer MARGIN = 30;
    public Float xPos;
    public Float yPos;
    public Float xSpeed = 0f;
    public Float ySpeed = 0f;
    public Float radius;
    public Canvas ballCanvas;
    public Long lastUpdate;
    public float []gravity = new float[3];
    public final Float BOUNCINESS = 0.5f; //Value from 0 to 1, higher bounces more
    public Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    public Ringtone ringtone;
    public final Integer HITRESPONSESPEED = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //Sensor initialisation
        SensorManager sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        SensorEventListener sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if (sensorEvent.sensor.getType() == Sensor.TYPE_GRAVITY) {
                    gravity = sensorEvent.values.clone();
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY), SensorManager.SENSOR_DELAY_GAME);

        //Width and height
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
        height = displayMetrics.heightPixels;

        //Set background
        ImageView background = findViewById(R.id.background);
        Paint paint = new Paint();
        Bitmap backgroundBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas backgroundCanvas = new Canvas(backgroundBitmap);
        paint.setColor(Color.BLACK);
        backgroundCanvas.drawRect(0, 0, width, height, paint);
        paint.setColor(Color.WHITE);
        backgroundCanvas.drawRect(MARGIN, MARGIN, width - MARGIN, height - MARGIN, paint);
        background.setImageBitmap(backgroundBitmap);

        //Set ringtone
        ringtone = RingtoneManager.getRingtone(getApplicationContext(), uri);

        //Draw ball
        final ImageView ballView = findViewById(R.id.ball);
        Bitmap ballBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        ballCanvas = new Canvas(ballBitmap);
        paint.setColor(Color.RED);
        xPos = width / 2f;
        yPos = height / 2f;
        radius = width * 0.05f;
        ballCanvas.drawCircle(xPos.intValue(), yPos.intValue(), radius, paint);
        ballView.setImageBitmap(ballBitmap);
        lastUpdate = System.currentTimeMillis();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        Thread.sleep(16);
                        updatePosition();
                        updateBall();
                        ballView.invalidate();
                    }
                } catch (InterruptedException error) {
                    Log.e("InterruptedException", "Error: " + error);
                }
            }
        }).start();
    }

    public void updateBall() {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Paint paint = new Paint();
                paint.setColor(Color.RED);
                ballCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                ballCanvas.drawCircle(xPos.intValue(), yPos.intValue(), radius, paint);
            }
        });
    }

    public void updatePosition() {
        Long deltaTime = System.currentTimeMillis() - lastUpdate;
        lastUpdate = System.currentTimeMillis();
        xSpeed += gravity[1] * deltaTime/100.0f;
        ySpeed += gravity[0] * deltaTime/100.0f;
        xPos += xSpeed;
        yPos += ySpeed;
        clampPosition();
    }

    public void clampPosition() {
        if (xPos < radius + MARGIN) {
            xPos = radius + MARGIN;
            xSpeed = -BOUNCINESS * xSpeed;
            if (xSpeed > HITRESPONSESPEED || xSpeed < -HITRESPONSESPEED) {
                playHitSound();
            }
        }else if (xPos > width - radius - MARGIN) {
            xPos = width - radius - MARGIN;
            xSpeed = -BOUNCINESS * xSpeed;
            if (xSpeed > HITRESPONSESPEED || xSpeed < -HITRESPONSESPEED) {
                playHitSound();
            }
        }
        if (yPos < radius + MARGIN) {
            yPos = radius + MARGIN;
            ySpeed = -BOUNCINESS * ySpeed;
            if (ySpeed > HITRESPONSESPEED || ySpeed < -HITRESPONSESPEED) {
                playHitSound();
            }
        }else if (ySpeed > HITRESPONSESPEED || ySpeed < -HITRESPONSESPEED) {
            yPos = height - radius - MARGIN;
            ySpeed = -BOUNCINESS * ySpeed;
            if (ySpeed > 5 || ySpeed < -5) {
                playHitSound();
            }
        }
    }

    public void playHitSound() {
        try{
            ringtone.play();
            Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(50);
        }catch (NullPointerException error) {
            Log.e("NullPointerException", "Error " + error);
        }
    }
}
